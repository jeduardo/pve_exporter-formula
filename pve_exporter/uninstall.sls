# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "pve_exporter/map.jinja" import pve_exporter with context %}

pve_exporter-remove-service:
  service.dead:
    - name: pve_exporter
  file.absent:
    - name: /etc/systemd/system/pve_exporter.service
    - require:
      - service: pve_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: pve_exporter-remove-service

pve_exporter-remove-binary:
  pip.installed:
    - name: prometheus-pve-exporter


pve_exporter-remove-config:
    file.absent:
      - name: {{ pve_exporter.config_dir }}
      - require:
        - pve_exporter-remove-binary
