{%- from "pve_exporter/map.jinja" import pve_exporter with context %}

pve_exporter-config:
  file.managed:
    - name: {{ pve_exporter.config_dir }}/pve.yml
    - source: salt://pve_exporter/files/pve.yml.j2
    - user: {{ pve_exporter.user }}
    - group: {{ pve_exporter.group }}
    - mode: 0640
    - template: jinja
    - context:
        pve_exporter: {{ pve_exporter }}
    - require:
      - file: {{ pve_exporter.config_dir }}
    {%- if pve_exporter.service != False %}
    - watch_in:
      - service: pve_exporter-service
    {%- endif %}

# Enabling the service here to ensure each state is independent.
pve_exporter-service:
  service.running:
    - name: pve_exporter
    # Restart service if config changes
    - restart: True
    - enable: {{ pve_exporter.service }}
